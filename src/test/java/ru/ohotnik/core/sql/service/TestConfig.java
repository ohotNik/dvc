package ru.ohotnik.core.sql.service;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

/**
 * @author Aleksander Okhonchenko.
 * @since 14.01.17.
 */
@ContextConfiguration
@ComponentScan(basePackages = "ru.ohotnik.core.sql.service")
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "ru.ohotnik.core.sql.repository")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@PropertySource(value = "application.properties")
public class TestConfig {
}
