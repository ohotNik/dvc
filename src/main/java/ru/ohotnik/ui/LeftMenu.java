package ru.ohotnik.ui;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

/**
 * @author ohotNik
 * @since 17.08.2016
 */
class LeftMenu extends VerticalLayout {

  LeftMenu(Navigator navigator) {
    super();

    Button button = new Button("Хосты");
    button.addClickListener((Button.ClickListener) event -> navigator.navigateTo(""));
    addComponent(button);
    button = new Button("Управление данными");
    button.addClickListener((Button.ClickListener) event -> navigator.navigateTo("script"));
    addComponent(button);

  }
}
