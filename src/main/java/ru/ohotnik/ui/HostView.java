package ru.ohotnik.ui;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

/**
 * @author aleksander.okhonchenko
 * @since 12.03.2017.
 */
public class HostView extends VerticalLayout implements View {

  public HostView() {
    super();
    setHeight("100%");
    setWidth("100%");
  }

  @Override
  public void enter(ViewChangeListener.ViewChangeEvent event) {
    Notification.show("Загрузка хостов");
  }
}
