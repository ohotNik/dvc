package ru.ohotnik.ui;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Panel;

/**
 * @author aleksander.okhonchenko
 * @since 27.02.2017.
 */
public class MenuPanel extends Panel {

  public MenuPanel(Navigator navigator) {
    super();
    addStyleName("dark");
    setWidth("250px");
    setHeight("100%");
    setContent(new LeftMenu(navigator));
  }
}
