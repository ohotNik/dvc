package ru.ohotnik.ui;

import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.StreamVariable;
import com.vaadin.ui.*;
import org.springframework.stereotype.Component;
import ru.ohotnik.App;
import ru.ohotnik.core.sql.domain.Script;
import ru.ohotnik.core.sql.service.DVCService;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * @author ohotNik
 * @since 18.08.2016
 */
@Component
public class DataManagementContent extends VerticalLayout implements View {

  private static DataManagementContent instance;
  private DVCService dvcService = App.look(DVCService.class);

  public DataManagementContent() {
    super();
    Label label = new Label("aaa");
    addComponent(label);
    Panel p = new Panel("Drag files here");
    p.setHeight("50px");
    DragAndDropWrapper dropWrapper = new DragAndDropWrapper(p);
    dropWrapper.setDropHandler(new DHandler());
    addComponent(dropWrapper);
    addComponent(dataTree());
  }

  static DataManagementContent getInstance() {
    if (instance == null) {
      instance = new DataManagementContent();
    }
    return instance;
  }

  private DVCService getDvcService() {
    if (dvcService == null) {
      dvcService = App.look(DVCService.class);
    }
    return dvcService;
  }

  private Tree dataTree() {
    Tree t = new Tree("Загруженные скрипты");
    if (getDvcService() == null) {
      return t;
    }
    List<Script> list = getDvcService().getScriptsByParent(null);
    for (Script script : list) {
      t.addItem(script.getName());
    }
    return t;
  }

  @Override
  public void enter(ViewChangeListener.ViewChangeEvent event) {
    Notification.show("Загрузка скриптов");
  }

  private class DHandler implements DropHandler {

    @Override
    public void drop(DragAndDropEvent event) {
      DragAndDropWrapper.WrapperTransferable tr = (DragAndDropWrapper.WrapperTransferable) event
          .getTransferable();
      Html5File[] files = tr.getFiles();
      Html5File file = files[0];
      if (!file.getFileName().endsWith(".sql")) {
        Notification.show("Неверное расширение файла.", Notification.Type.TRAY_NOTIFICATION);
        return;
      }
      final ByteArrayOutputStream bas = new ByteArrayOutputStream();
      StreamVariable streamVariable = new StreamVariable() {

        @Override
        public OutputStream getOutputStream() {
          return bas;
        }

        @Override
        public boolean listenProgress() {
          return false;
        }

        @Override
        public void onProgress(StreamingProgressEvent event) {

        }

        @Override
        public void streamingStarted(StreamingStartEvent event) {

        }

        @Override
        public void streamingFinished(StreamingEndEvent event) {
          boolean b = getDvcService().addScript(bas.toString(), event.getFileName());
          if (!b) {
            Notification.show("Что-то не так. Возможно, скрипт уже существует.");
          }
        }

        @Override
        public void streamingFailed(StreamingErrorEvent event) {

        }

        @Override
        public boolean isInterrupted() {
          return false;
        }
      };
      file.setStreamVariable(streamVariable);
    }

    @Override
    public AcceptCriterion getAcceptCriterion() {
      return AcceptAll.get();
    }
  }
}
