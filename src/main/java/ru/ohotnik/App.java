package ru.ohotnik;

import com.vaadin.spring.annotation.EnableVaadin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author ohotNik
 * @since 13.08.2016
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableVaadin
@ComponentScan(basePackages = "ru.ohotnik")
@EnableJpaRepositories(basePackages = "ru.ohotnik.core.sql.repository")
public class App {

  private static final int maxRetries = 5;
  private static ConfigurableApplicationContext context;

  public static void main(String[] args) throws InterruptedException {
    //todo wait docker mysql. fix it in docker
    int tries = 0;
    while (tries <= maxRetries) {
      try {
        context = SpringApplication.run(App.class, args);
        return;
      } catch (final Exception ex) {
        if (tries > maxRetries)
          throw ex;
        tries++;
        Thread.sleep(100000);
      }

    }
  }

  public static <T> T look(Class<? extends T> clazz) {
    return context == null ? null : context.getBean(clazz);
  }

}
