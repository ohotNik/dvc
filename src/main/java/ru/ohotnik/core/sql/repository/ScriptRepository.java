package ru.ohotnik.core.sql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ohotnik.core.sql.domain.Script;

import java.util.List;

/**
 * @author ohotNik
 * @since 20.08.2016
 */
@Repository
public interface ScriptRepository extends JpaRepository<Script, Integer> {

  Script findOneByName(String name);

  List<Script> findAllByParent(Integer parent);

}
