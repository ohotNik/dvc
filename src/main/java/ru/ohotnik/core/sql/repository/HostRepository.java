package ru.ohotnik.core.sql.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ohotnik.core.sql.domain.Host;

/**
 * @author Aleksander Okhonchenko.
 * @since 14.01.17.
 */
public interface HostRepository extends JpaRepository<Host, Long> {

    Host findOneByUrl(final String url);

}
