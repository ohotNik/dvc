package ru.ohotnik.core.sql.service;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ohotnik.core.sql.domain.Script;
import ru.ohotnik.core.sql.repository.ScriptRepository;

import java.util.List;

/**
 * @author ohotNik
 * @since 20.08.2016
 */
@Service
@Log
public class DVCService {

  @Autowired
  private ScriptRepository scriptRepository;

  public boolean addScript(String script, String fileName) {
    Script oneByName = scriptRepository.findOneByName(fileName);
    if (oneByName != null) {
      log.warning("Скрипт уже существует. Загрузка невозможна.");
      return false;
    }
    Script s = new Script();
    s.setName(fileName);
    s.setValue(script);
    scriptRepository.save(s);
    return true;
  }

  public List<Script> getScriptsByParent(Integer parentId) {
    return scriptRepository.findAllByParent(parentId);
  }

}
