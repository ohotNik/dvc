package ru.ohotnik.core.sql.service;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import ru.ohotnik.core.sql.domain.Host;
import ru.ohotnik.core.sql.repository.HostRepository;

import javax.transaction.Transactional;
import java.util.logging.Level;

/**
 * @author Aleksander Okhonchenko.
 * @since 13.01.17.
 */
@Log
@Service
public class InitService {

    @Autowired
    private HostRepository hostRepository;

    /**
     * инициализация новой таблицы с информацией о миграции.
     *
     * @param url           адрес площадки.
     */
    @Transactional
    public void initDbInfo(final String url) {
        Host host = hostRepository.findOneByUrl(url);
        if (host == null) {
            log.log(Level.INFO, "Host was not found. Created new one.");
            host = new Host();
            host.setUrl(url);
            host.setLastScript(-1L);
            hostRepository.save(host);
        }
    }

}
