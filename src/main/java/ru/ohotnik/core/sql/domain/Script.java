package ru.ohotnik.core.sql.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ohotNik
 * @since 20.08.2016
 */
@Entity
@Table(name = "script")
@Data
public class Script {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;
  private String name;
  @Column(columnDefinition = "TEXT")
  private String value;
  @Column(columnDefinition = "INT(10) default 0")
  private int depth;
  private Integer parent;


}
