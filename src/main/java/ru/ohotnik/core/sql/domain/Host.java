package ru.ohotnik.core.sql.domain;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author Aleksander Okhonchenko.
 * @since 14.01.17.
 */
@Entity
@Table(name = "hosts")
@ToString
@Data
public class Host {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true, nullable = false)
    private String url;
    private Long lastScript;

}
