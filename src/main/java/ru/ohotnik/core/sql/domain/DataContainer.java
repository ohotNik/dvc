package ru.ohotnik.core.sql.domain;

import lombok.Data;

import javax.persistence.*;

/**
 * @author aleksander.okhonchenko
 * @since 18.02.2017.
 */
@Entity
@Table(name = "dc")
@Data
public class DataContainer {

  @Id
  private Long id;
  @JoinColumn(name = "script_id")
  @ManyToOne
  private Script script;
  @JoinColumn(name = "host_id")
  @ManyToOne
  private Host host;
  private boolean isOk;
  private String error;

}
