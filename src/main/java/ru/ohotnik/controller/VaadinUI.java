package ru.ohotnik.controller;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import ru.ohotnik.ui.CommonView;
import ru.ohotnik.ui.DataManagementContent;
import ru.ohotnik.ui.HostView;
import ru.ohotnik.ui.MenuPanel;

/**
 * @author ohotNik
 * @since 13.08.2016
 */
@SpringUI(path = "/")
@DesignRoot
public class VaadinUI extends UI {

  @Override
  protected void init(VaadinRequest request) {
    HorizontalLayout mainLayout = new HorizontalLayout();
    mainLayout.setHeight("100%");
    mainLayout.setMargin(true);
    mainLayout.setSpacing(true);
    final CommonView content = new CommonView();
    Navigator navigator = new Navigator(this, content);
    mainLayout.addComponent(new MenuPanel(navigator));
    mainLayout.addComponent(content);
    setContent(mainLayout);

    navigator.addView("", new HostView());
    navigator.addView("script", new DataManagementContent());
  }
}
