FROM openjdk:8-jdk
MAINTAINER Aleksander Okhonchenko <strunasa@mail.ru>


COPY target/dvc-1.0-SNAPSHOT.jar /opt/dvc/dvc-1.0-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "/opt/dvc/dvc-1.0-SNAPSHOT.jar"]